package DB

/**
  * Created by diana on 9/8/17.
  */
trait PostgreComponent extends DBComponent {
  val driver = slick.driver.PostgresDriver
  import driver.api._
  val db: Database = PostgreDB.connectionPool
}

object PostgreDB {
  import slick.driver.PostgresDriver.api._
  val connectionPool = Database.forConfig("postgreDB")
}
