package DB
import Models._

/**
  * Created by diana on 9/8/17.
  */

trait VacancyRepository extends Schema with VacancyRepositoryComponent { this:DBComponent=>
  import driver.api._
  import scala.concurrent.ExecutionContext.Implicits.global

  override def create(vacancy: Vacancy) = {
    val createQuery = vacancies+=vacancy
    db.run(createQuery).map {
      r => ResultOfCreate(r)
    }
  }

  override def getAll() = {
    val getAllQuery = vacancies.result
    db.run(getAllQuery).map{
      r=> ResultOfGet(r)
    }
  }

  override def update(vacancy: Vacancy) = {
    val updateQuery = vacancies.filter(_.id === vacancy.id).update(vacancy)
    db.run(updateQuery).map{
      r=> ResultOfUpdate(r)
    }
  }

  override def delete(id: Int) = {
    val deleteQuery =  vacancies.filter(_.id === id).delete
    db.run(deleteQuery).map{
      r=> ResultOfDelete(r)
    }
  }
}
