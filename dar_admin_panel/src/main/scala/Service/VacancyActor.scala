package Service

import java.io.{BufferedOutputStream, FileOutputStream}
import java.text.SimpleDateFormat
import java.util.{Base64, Calendar}

import DB.{PostgreComponent, VacancyRepository}
import Models._
import Routing.ApiMessage
import akka.actor.{Actor, ActorLogging, ActorSystem}
import akka.pattern.pipe
import com.typesafe.config.ConfigFactory
import VacancyActor._

/**
  * Created by diana on 8/21/17.
  */
object VacancyActor {
  case class GetVacancy() extends ApiMessage
  case class DeleteVacancy(id:Int) extends ApiMessage
  case class UpdateVac(id:Option[Int], name:String, responsibility:String, demands:String, conditions:String, image:String, image_name:String, date:String) extends ApiMessage
  case class CreateVacancy(id:Option[Int], name:String, responsibility:String, demands:String, conditions:String, image:String, image_name:String, date:Option[String]) extends ApiMessage
}

class VacancyActor extends Actor with ActorLogging {

  val config = ConfigFactory.load()
  implicit val system = ActorSystem()
  implicit val executionContext = system.dispatcher


  object vacancyRepository extends VacancyRepository with PostgreComponent


    def receive = {

      case m: CreateVacancy =>

        val format = new SimpleDateFormat("d-M-y")
        println(format.format(Calendar.getInstance().getTime()))
        val date=format.format(Calendar.getInstance().getTime())

        val image_name=m.image_name
        val image = m.image
        val path = s"../static/$image_name"
        decoder(image, path)

        val vacancy: Vacancy = Vacancy(m.id, m.name, m.responsibility, m.demands, m.conditions,  path, Option(date))
        vacancyRepository.create(vacancy)
        val result = vacancyRepository.getAll()
        result.pipeTo(self)

        println("Vacancy is here", vacancy)

      case m: GetVacancy =>
        val allVac = vacancyRepository.getAll()
        allVac.pipeTo(self)


      case m: DeleteVacancy =>
        vacancyRepository.delete(m.id)
        val delVac = vacancyRepository.getAll()
          delVac.pipeTo(self)

      case m: UpdateVac =>

        val format = new SimpleDateFormat("d-M-y")
        println(format.format(Calendar.getInstance().getTime()))
        val date=format.format(Calendar.getInstance().getTime())

        val image_name=m.image_name
        val image = m.image
        val path = s"../static/$image_name"
        decoder(image, path)

        val upVac = Vacancy(m.id, m.name, m.responsibility, m.demands, m.conditions, path, Option(date))
        vacancyRepository.update(upVac)
        val result = vacancyRepository.getAll()
        result.pipeTo(self)


      case m: ResultOfGet =>
        context.parent ! m


      case m =>
        log.info(s"Invalid message: $m")

    }


  def decoder(imageString: String, path:String) {
    val imageOutFile = new BufferedOutputStream(new FileOutputStream(path))

    val imageByteArray = Base64.getDecoder().decode(imageString);
    imageOutFile.write(imageByteArray);
    imageOutFile.close()
  }

  }
