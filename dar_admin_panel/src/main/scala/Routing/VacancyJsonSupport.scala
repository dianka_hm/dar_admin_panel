package Routing

import Models.{Error, ResultOfCreate, ResultOfGet, Vacancies, Vacancy}
import Service.VacancyActor.{CreateVacancy, GetVacancy, UpdateVac}
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.DefaultJsonProtocol


/**
  * Created by diana on 8/21/17.
  */
object VacancyJsonSupport extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val VacancyFormats = jsonFormat7(Vacancy)
  implicit val VacanciesFormats = jsonFormat1(Vacancies)
  implicit val ErrorFormats=jsonFormat2(Error)
  implicit val createVacancyFormats = jsonFormat8(CreateVacancy)
  implicit val getVacancyFormats =jsonFormat0(GetVacancy)
  implicit val updateVacFormats =jsonFormat8(UpdateVac)
  implicit val ResultOfGetFormats =jsonFormat1(ResultOfGet)
  implicit val resultOfCreateFormats = jsonFormat1(ResultOfCreate)
}
