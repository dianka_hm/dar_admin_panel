package Models

import scala.concurrent.Future


/**
  * Created by diana on 9/11/17.
  */
case class ResCreateVacancy (id:Option[Int], name:String, responsibility:String, demands:String, conditions:String)

case class ResGetVacancy(allVac:String)

case class ResDeleteVacancy(id:Int)

case class ResUpdateVacancy()

