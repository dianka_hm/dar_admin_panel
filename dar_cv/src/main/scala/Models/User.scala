package Models


/**
  * Created by diana on 8/21/17.
  */
case class User(id:Option[Int],
                name: String,
                surname:String,
                city:String,
                email:String,
                letter:String,
                file_name:List[String],
                cv_file:List[String]){

}

