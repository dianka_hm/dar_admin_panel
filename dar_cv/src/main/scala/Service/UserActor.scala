package Service

import java.io.{BufferedOutputStream, FileOutputStream}
import java.util.Base64
import Models._
import Routing.ApiMessage
import Service.UserActor.CreateUser
import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import com.typesafe.config.ConfigFactory

import scala.concurrent.Future

/**
  * Created by diana on 8/21/17.
  */

object UserActor {
  case class CreateUser(id:Option[Int], name:String, surname:String, city:String, email:String, letter:String, file_name:List[String], cv_file:List[String]) extends ApiMessage
}

class UserActor extends Actor with ActorLogging {

  val config = ConfigFactory.load()
  implicit val system = ActorSystem()
  implicit val executionContext = system.dispatcher


  val emailActor = system.actorOf(Props[GmailSenderActor], "emailActor")


  def receive = {

      case m: CreateUser =>

        var paths = List.empty[String]

        val files_name = m.file_name
        val files_cv = m.cv_file
        for (i <- 0 to files_name.size-1) {

          val f_name=files_name(i)
          val path = s"../static/$f_name"
          decoder(files_cv(i), path)
          paths = paths:+(path)

        }


        val user: User = User(m.id, m.name, m.surname, m.city, m.email, m.letter, m.file_name, paths)
        emailActor ! user
        context.parent ! "Ok"


      case m =>
        log.info(s"Invalid message: $m")

    }


  def decoder(imageString: String, path:String) {
    val imageOutFile = new BufferedOutputStream(new FileOutputStream(path))

    val imageByteArray = Base64.getDecoder().decode(imageString);
    imageOutFile.write(imageByteArray);
    imageOutFile.close()

  }
}
