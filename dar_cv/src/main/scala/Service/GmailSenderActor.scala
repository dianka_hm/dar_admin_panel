package Service
import java.util._
import javax.mail._
import javax.mail.internet._

import Models.{User}
import akka.actor.{Actor, ActorLogging}


/**
  * Created by diana on 10/24/17.
  */

class GmailSenderActor extends Actor with ActorLogging {

  def receive = {

    case user: User =>      val name=user.name
                            val surname=user.surname
                            val city = user.city
                            val email = user.email
                            val letter=user.letter
                            val cv_file = user.cv_file

    print("User", user)

    val username = "Vacancy@dar.kz"
    val password = "U$er1234"

    val properties = new Properties()
    properties.put("mail.smtp.host", "smtp.gmail.com")
    properties.put("mail.smtp.starttls.enable", "true")
    properties.put("mail.smtp.port", "587")


    val message = new MimeMessage(Session.getDefaultInstance(properties))
    val multipart = new MimeMultipart()
    val alternative = new MimeMultipart("alternative")


    val textPart, wrap = new MimeBodyPart()

    textPart.setText(s"$surname $name  $city $email $letter")
    alternative.addBodyPart(textPart)
    wrap.setContent(alternative)
    multipart.addBodyPart(wrap)


   cv_file.foreach { file=>
     val atPart = new MimeBodyPart()
     atPart.attachFile(file)
       multipart.addBodyPart(atPart)
  }


   message.setFrom(s"<$username>")
   message.addRecipients(Message.RecipientType.TO, s"<$username>")
   message.setSubject("DAR CV")
   message.setContent(multipart)

   Transport.send(message, username, password)
  }
}