package Routing

import Service.UserActor
import Service.UserActor.CreateUser
import akka.actor.{ActorSystem, Props}
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import akka.util.Timeout
import com.mongodb.casbah.commons.conversions.scala.Serializers

import scala.concurrent.duration.{Duration, _}


/**
  * Created by diana on 8/21/17.A
  */

object Routing extends  RequestHandlerMaker with Serializers {


  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()

  implicit val timeout: Timeout = 5.seconds

  val userActor = Props(new UserActor())


  val route = pathPrefix("user") {
    import UserJsonSupport._

    post {
      entity(as[CreateUser]) { mes =>
        RequestHandler.imperativelyComplete { ctx =>
          handle(ctx, userActor, mes)
        }
      }
    }
  }
}


