package Routing

import Models._
import Service.UserActor.{CreateUser}
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.DefaultJsonProtocol


/**
  * Created by diana on 8/21/17.
  */
object UserJsonSupport extends DefaultJsonProtocol with SprayJsonSupport {
   implicit val userFormats = jsonFormat8(User)

   implicit val createUserFormats = jsonFormat8(CreateUser)

}
