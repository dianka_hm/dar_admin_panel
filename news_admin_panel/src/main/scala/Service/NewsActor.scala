package Service

import java.io.{BufferedOutputStream, FileOutputStream}
import java.text.SimpleDateFormat
import java.util.{Base64, Calendar}

import DB.{NewsRepository, PostgreComponent}
import Models._
import Routing.ApiMessage
import akka.actor.{Actor, ActorLogging, ActorSystem}
import akka.pattern.pipe
import com.typesafe.config.ConfigFactory
import NewsActor._

import scala.concurrent.Future

/**
  * Created by diana on 8/21/17.
  */
object NewsActor {
  case class GetNews() extends ApiMessage
  case class DeleteNews(id:Int) extends ApiMessage
  case class UpdateNews(id:Option[Int], title:String, content:String, position:Int, image:String, image_name:String, view:Int, date:Option[String], news_type:String) extends ApiMessage
  case class CreateNews(id:Option[Int], title:String, content:String, position:Int, image:String, image_name:String, view:Int, date:Option[String], news_type:String) extends ApiMessage
}

class NewsActor extends Actor with ActorLogging {

  val config = ConfigFactory.load()
  implicit val system = ActorSystem()
  implicit val executionContext = system.dispatcher

  object newsRepository extends NewsRepository with PostgreComponent


    def receive = {
      case m: CreateNews =>
        val image_name=m.image_name
        val image = m.image
        val path = s"../static/$image_name"
        decoder(image, path)

        val format = new SimpleDateFormat("d.M.y")
        println(format.format(Calendar.getInstance().getTime()))
        val date=format.format(Calendar.getInstance().getTime())

        val news: News = News(m.id, m.title, m.content, m.position, m.image_name, path, m.view, Option(date), m.news_type)
        newsRepository.create(news)

        val result = newsRepository.getAll()
        result.pipeTo(self)

      case m: GetNews =>
        val allNews = newsRepository.getAll()
        allNews.pipeTo(self)

      case m: DeleteNews =>
        newsRepository.delete(m.id)
        val delNews = newsRepository.getAll()
        delNews.pipeTo(self)

      case m: UpdateNews =>
        val image_name=m.image_name
        val image = m.image
        val path = s"../static/$image_name"
        decoder(image, path)

        val format = new SimpleDateFormat("d.M.y")
        println(format.format(Calendar.getInstance().getTime()))
        val date=format.format(Calendar.getInstance().getTime())

        val upNews = News(m.id, m.title, m.content, m.position, path, m.image_name, m.view+1, Option(date), m.news_type)
        println("Update ", upNews)
        newsRepository.update(upNews)
        val result = newsRepository.getAll()
        result.pipeTo(self)


      case m: ResultOfGet =>
        context.parent ! m

      case m =>
        log.info(s"Invalid message: $m")

    }


  def decoder(imageString: String, path:String) {
    val imageOutFile = new BufferedOutputStream(new FileOutputStream(path))

    val imageByteArray = Base64.getDecoder().decode(imageString);
    imageOutFile.write(imageByteArray);
    imageOutFile.close()

  }

}
