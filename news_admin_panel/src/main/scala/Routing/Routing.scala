package Routing

import Service.NewsActor
import akka.actor.{Actor, ActorSystem, Props}
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import akka.util.Timeout
import Service.NewsActor._
import com.mongodb.casbah.commons.conversions.scala.Serializers

import scala.concurrent.duration.{Duration, _}

/**
  * Created by diana on 8/21/17.A
  */

object Routing extends  RequestHandlerMaker with Serializers  {


    implicit val system = ActorSystem()
    implicit val materializer = ActorMaterializer()

    implicit val timeout: Timeout = 5.seconds

    val newsActor = Props(new NewsActor())


  val route = pathPrefix("news") {
      import NewsJsonSupport._

    post {
      entity(as[CreateNews]) { mes =>
        RequestHandler.imperativelyComplete { ctx =>
          handle(ctx, newsActor, mes)
        }
       }
      }~
     get{
       RequestHandler.imperativelyComplete { ctx =>
          handle(ctx, newsActor, NewsActor.GetNews())
       }
     }~
      delete{
        path(Segment) { (id) =>
          RequestHandler.imperativelyComplete{ ctx=>
            handle(ctx, newsActor, NewsActor.DeleteNews(id.toInt))
          }
        }
      }~
    put{
      entity(as[UpdateNews]) { mes =>
        RequestHandler.imperativelyComplete{ ctx=>
          handle(ctx, newsActor, mes)
        }
      }
    }
  }
}

