package Routing

import Models.{News, ResultOfCreate, ResultOfGet}
import Service.NewsActor._
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.DefaultJsonProtocol


/**
  * Created by diana on 8/21/17.
  */
object NewsJsonSupport extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val NewsFormats = jsonFormat9(News)
  implicit val createNewsFormats = jsonFormat9(CreateNews)
  implicit val getNewsFormats =jsonFormat0(GetNews)
  implicit val updateNewsFormats =jsonFormat9(UpdateNews)
  implicit val ResultOfGetFormats =jsonFormat1(ResultOfGet)
  implicit val resultOfCreateFormats = jsonFormat1(ResultOfCreate)
}
