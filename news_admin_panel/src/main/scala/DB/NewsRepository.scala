package DB
import Models._

/**
  * Created by diana on 9/8/17.
  */

trait NewsRepository extends Schema with NewsRepositoryComponent { this:DBComponent=>
  import driver.api._
  import scala.concurrent.ExecutionContext.Implicits.global

  override def create(news: News) = {

    //val old_value=newsTable.map(x=>(x.position)).result

    //val query= newsTable.filter(_.position >= news.position ).map(_.position).update()


   // val q = newsTable.map(p => (p.position))



    val updateAction = newsTable.filter(_.position >= news.position).map(x => (x.position)).update(99)
    db.run(updateAction)



   // db.run(sql"""SELECT * FROM #$newsTable""")

    //db.run(sql"""select position from $newsTable """.as[(Int)]).map{r=>ResultOfCreate(r)}


    //val updateAction = coffees.filter(_.name === "Espresso").map(c => (c.price, c.name)).update((10.49, "Cappuccino"))

    //position + 1

   // val updateQu = newsTable.

    val createQuery= newsTable+=news

    db.run(createQuery).map {
      r => ResultOfCreate(r)
    }

    //val sortQuery = newsTable.sortBy(_.position.desc.nullsFirst).result


   // val q3 = newsTable.sortBy(_.position.desc.nullsFirst).result

    //db.run(sortQuery)
    //db.run(createQuery)

    //val sortQuery=newsTable.sortBy(r=>r.position)

    //val resQuery=newsTable.result
  }

  override def getAll() = {

    val getAllQuery = newsTable.sortBy(_.position).result
    db.run(getAllQuery).map{
      r=> ResultOfGet(r)
    }
  }

  override def update(news: News) = {
    print("Update news here", news)
    val updateQuery = newsTable.filter(_.id === news.id).update(news)
    db.run(updateQuery).map{
      r=> ResultOfUpdate(r)
    }
  }

  override def delete(id: Int) = {
    val deleteQuery =  newsTable.filter(_.id === id).delete
    db.run(deleteQuery).map{
      r=> ResultOfDelete(r)
    }
  }
}
