package DB

import Models.News

/**
  * Created by diana on 9/8/17.
  */
trait Schema { this:DBComponent=>
  import driver.api._

  class NewsTable(tag: Tag) extends Table[News](tag, "news") {
    def id = column[Int] ("id", O.PrimaryKey, O.AutoInc,  O.Default(0))
    def title = column[String]("title")
    def content = column[String]("content")
    def position = column[Int]("position")
    def image = column[String]("image")
    def image_name = column[String]("image_name")
    def view = column[Int]("view")
    def date = column[Option[String]]("date")
    def news_type= column[String]("news_type")

    def * = (id.?, title, content, position, image, image_name, view, date, news_type) <> (News.tupled, News.unapply)
  }

  lazy val newsTable = TableQuery[NewsTable]
}
