package Models


/**
  * Created by diana on 8/21/17.
  */
case class News(id:Option[Int],
                title: String,
                content:String,
                position:Int,
                image:String,
                image_name:String,
                view:Int,
                date:Option[String],
                news_type:String)

case class ResultOfDelete(result: Int)

case class ResultOfUpdate(result:Int)

case class ResultOfCreate(result: Int)

case class ResultOfGet(result: Seq[News])

